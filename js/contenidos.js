function actualizar(seccion)
{
    $("#main").addClass("actualizando");
      
    setTimeout(function() {
        $("#main").empty();
        
        switch (seccion)
        {
            case 0:
            default:
                inicio();
                break;
                
            case 1:
                infancia();
                break;
                
            case 2:
                manco();
                break;
                
            case 3:
                cautiverio();
                break;
        }
        
        $("#main").removeClass("actualizando");
    }, 1000);
}

function cautiverio()
{
    /* 
    El operador !! convertirá el valor a boolean
    Este será usado para impedir que la posición de las imágenes pueda coincidir
    */
    var posicion = !!Math.floor(Math.random() * (1 - 0 + 1) + 0);
        
    $("#main").append($("<div>").addClass("jumbotron").append($("<h1>").text("Cautiverio")));
    
    // El operador + convierte el valor de boolean a integer
    $("#main").append($("<img>").attr("src", "images/02_Cervantes_1738.jpg").addClass("img" + +posicion));
    
    $("#main").append($("<p>").text("Tras ser capturado por Arnaut Mamí, fué llevado a Argel como esclavo, donde padeció un cautiverio de cinco años, dentro de los cuales se constatan cuatro intentos de fuga por su parte, todos ellos fallidos."));
    
    $("#main").append($("<p>").text("Cabe destacar que, en estos cuatro intentos de fuga, realizados dos por tierra y dos por mar, Miguel de Cervantes asumió siempre la responsabilidad exclusiva de los mismos."));
    
    $("#main").append($("<p>").text("Finalmente, el 19 de septiembre de 1580, al precio de 500 ducados, fué comprada su libertad, pudiendo así volver a España, donde, tras ser incapaz de obtener la recompensa por los servicios prestados, abandonó la carrera militar."));
    
    // Aquí convertimos a integer el opuesto al utilizado antes
    $("#main").append($("<img>").attr("src", "images/cervantes1.png").addClass("img" + +!posicion));
    
    $("#main").append($("<p>").html("Habiendo finalizado dicha etapa de su vida, se aventuró a volver a la escritura, lo que pronto daría lugar a obras tales como <span class=\"ital\">La Galatea</span>, <span class=\"ital\">El ingenioso hidalgo don Quijote de La Mancha</span>, <span class=\"ital\">Novelas Ejemplares</span> y <span class=\"ital\">Viaje al Parnaso</span>, entre otras."));
    
    $("#main").append($("<p>").text("Miguel de Cervantes falleció entre el 22 y el 23 de abril de 1616, en su casa de Madrid.\nA día de hoy, todavía se desconoce el verdadero aspecto de su rostro, disponiendo únicamente de interpretaciones del mismo."));
}

function infancia()
{    
    $("#main").append($("<div>").addClass("jumbotron").append($("<h1>").text("Infancia")));
    
    $("#main").append($("<p>").text("Se desconoce la fecha exacta del nacimiento de Miguel de Cervantes, pero se estima el 29 de septiembre de 1547, puesto que sí se sabe con certeza cuando fué bautizado: 9 de octubre de 1547."));
    
    $("#main").append($("<img>").attr("src", "images/03_Cervantes_estatua_1892.jpg").addClass("img" + Math.floor(Math.random() * (1 - 0 + 1) + 0))); // Decidir la posición de la imagen aleatoriamente
    
    $("#main").append($("<p>").text("Fué el tercero de cinco hijos, con dos hermanas mayores llamadas Andrea y Luisa, y dos hermanos menores llamados Rodrigo y Magdalena. La información disponible sobre sus primeros veinte años de vida no es completamente certera, más que la precaria familiar situación y los movimientos que su padre realizó antes de asentarse en Madrid."));
    
    $("#main").append($("<p>").html("También, debido a la precisión de la descripción sobre un colegio de jesuitas que realizó en su novela <span class=\"ital\">El coloquio de los perros</span>, se puede suponer que estudiase en un colegio de jesuitas."));
    
    $("#main").append($("<p>").text("Lamentablemente, su padre, Rodrigo, se vió obligado a huir continuamente de acreedores, abandonando Alcalá de Henares para tratar de hallar la fortuna en la aquel entonces esplendorosa Valladolid, hasta que acabase por incurrir siete meses de cárcel en 1552 con motivo de impagos, tras cuyo cumplimiento, en 1553, decidió asentarse en Córdoba, lugar en el cual presuntamente Miguel de Cervantes fuese matriculado para estudiar en un colegio de jesuitas."));
}

function inicio()
{    
    $("#main").append($("<div>").addClass("jumbotron").append($("<h1>").text("Introducción")));
    
    $("#main").append($("<div>").addClass("flex-p"));
    
    $(".flex-p").append($("<p>").text("Nacido en Alcalá de Henares, Miguel de Cervantes, máximo exponente de la literatura Española, aún sigue siendo todo un misterio a día de hoy."));
    
    $(".flex-p").append($("<p>").text("Si bien se saben algunos detalles sobre la vida de Cervantes, por otro lado, muchas otras cosas son desconocidas. Así, sabemos, por ejemplo, que fué bautizado el 9 de octubre de 1547, y, si bien se dispone de una autodescripción física, no existe ninguna reproducción que nos muestre su verdadero aspecto."));
    
    $(".flex-p").append($("<p>").html("Esta página está dedicada al autor de la famosísima obra <span class=\"ital\">El Quijote</span>, Miguel de Cervantes."));
    
    $(".flex-p").append($("<div>").append($("<img>").attr("src", "images/04_Cervantes_Lepanto_2016.jpg").attr("id", "intro")));
}

function manco()
{
    $("#main").append($("<div>").addClass("jumbotron").append($("<h1>").text("El Manco de Lepanto")));
    
    $("#main").append($("<p>").text("En 1569, con motivo de una herida sufrida por Antonio de Sigura en un duelo, Miguel de Cervantes fué condenado a que le cortaran públicamente la mano derecha. Esto justificaría la huida de Miguel de Cervantes hacia Italia, más en concreto Roma, a donde llegó en diciembre del mismo año con un certificado de cristiano viejo (sin ascendientes conversos de otras religiones), aunque nunca presentó pruebas tangibles para la obtención del mismo."));
    
    $("#main").append($("<p>").text("Sin embargo, por aquella época, el conflicto entre la Santa Alianza (España, Roma y Venecia) contra los turcos estallaría una vez más, llevando así a Miguel de Cervantes a alistarse en la compañía de Diego de Urbina, misma compañía en la que ya militaba su hermano, Rodrigo."));
    
    $("#main").append($("<div>").addClass("media-middle").append($("<img>").attr("src", "images/Miguel de Cervantes para JM Marin.jpg").addClass("media-object img" + Math.floor(Math.random() * (1 - 0 + 1) + 0))));
    
    $("#main").append($("<p>").html("Habiendo sido partícipe de la batalla de Lepanto, descrito por haber <span class=\"ital\">combatido muy valientemente</span> por sus propios compañeros, recibió dos disparos en el pecho durante el transcurso de la misma, llegando entonces un tercero a inutilizarle la mano izquierda permanentemente, otorgándole así el sobrenombre de <span class=\"ital\">El Manco de Lepanto</span>, hecho que citó en el prólogo al Quijote:"));
    
    $("#main").append($("<p>").addClass("ital").text("Lo que no he podido dejar de sentir es que me note de viejo y de manco, como si hubiera sido en mi mano haber detenido el tiempo, que no pasase por mí, o si mi manquedad hubiera nacido en alguna taberna, sino en la más alta ocasión que vieron los siglos pasados, los presentes, ni esperan ver los venideros. Si mis heridas no resplandecen en los ojos de quien las mira, son estimadas, a lo menos, en la estimación de los que saben dónde se cobraron; que el soldado más bien parece muerto en la batalla que libre en la fuga; y es esto en mí de manera, que si ahora me propusieran y facilitaran un imposible, quisiera antes haberme hallado en aquella facción prodigiosa que sano ahora de mis heridas sin haberme hallado en ella."));
    
    $("#main").append($("<p>").html("Tras recuperarse de las heridas sufridas en la batalla de Lepanto, aún participó en batallas realizadas, también bajo el mando de Juan de Austria, hermano bastardo del rey, en Navarino, Corfú y Túnez. Cuando por fin se decidió a regresar a España a fin de ser premiado por los servicios militares prestados, disponiendo en su haber de cartas de recomendación del propio Juan de Austria y del duque de Sessa, la galera en la que viajaba, <span class=\"ital\">El Sol</span>, fué capturada por el corsario Arnaut Mamí el 26 de septiembre de 1575."));
}

$(document).ready(function ()
{
    inicio();
});
