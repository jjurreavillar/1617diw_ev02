Juan José Urrea Villar, grupo H. Diseño de interfaces WEB (2º DAW).
Las imágenes han sido obtenidas de CEEDCV, DIW 2º DAW.
La información ha sido extraída de http://www.biografiasyvidas.com/monografia/cervantes/ y de http://www.cervantesvirtual.com/portales/miguel_de_cervantes/autor_biografia/

Se han utilizado los siguientes recursos con fines orientativos:
https://css-tricks.com/approaches-media-queries-sass/
http://getbootstrap.com/examples/starter-template/
http://getbootstrap.com/examples/sticky-footer/
Materiales varios de DIW, 2º DAW, CEEDCV.
